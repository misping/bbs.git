<?php

namespace App\Application\Comment\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'bbs_reply';

    protected $fillable = [
        'id',
        'comment_id',
        'user_id',
        'comment',
        'content_id',
    ];
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
