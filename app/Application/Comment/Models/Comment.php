<?php

namespace App\Application\Comment\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'bbs_comment';

    protected $fillable = [
        'id',
        'recovery_num',
        'user_id',
        'comment',
        'content_id',
        "give_num"
    ];
    protected $appends = ['last_reply_time'];
    public function getLastReplyTimeAttribute()
    {
        return Reply::where("comment_id", $this->id)->orderBy("id", 'desc')->value("created_at");
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function infoReply()
    {
        return $this->hasMany(Reply::class, 'comment_id', 'id');
    }
}
