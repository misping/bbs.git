<?php

namespace App\Application\Auth;

use Composer\Application\Auth\RoleClient as BaseRuleClient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RoleClient extends BaseRuleClient
{

    public function __construct()
    {
        parent::__construct();
        $this->validateMessage = [
            'required' => '输入角色名称',
        ];
    }
    public function handleValidate()
    {
        Validator::make(
            $this->data,
            [
                'name' => [
                    'required',
                    Rule::unique(config('permission.models.role'), 'name'),
                ],
            ],
            $this->validateMessage
        )->validate();
    }

    public function handleUpdateValidate()
    {
        Validator::make(
            $this->data,
            ['name' => [
                Rule::unique(config('permission.models.role'), 'name')->ignore($this->id),
            ]],
            $this->validateMessage
        )->validate();
    }
}
