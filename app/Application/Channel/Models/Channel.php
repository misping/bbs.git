<?php

namespace App\Application\Channel\Models;

use App\Application\Content\Models\Content;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'bbs_channel';

    protected $fillable = [
        'id',
        'title',
        'sort',
        "content_num"
    ];
    protected $appends = ['last_content_time', 'users_num'];
    public function getLastContentTimeAttribute()
    {
        return Content::where("channel_id", $this->id)->orderBy("id", 'desc')->value("created_at");
    }
    public function getUsersNumAttribute()
    {
        return Content::where("channel_id", $this->id)->distinct("user_id")->count();
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
