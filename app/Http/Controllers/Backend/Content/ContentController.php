<?php

namespace App\Http\Controllers\Backend\Content;


use App\Application\Content\Client;
use App\Application\Content\Models\Content;
use Spatie\QueryBuilder\AllowedFilter;

class ContentController extends Client
{
    public function __construct(Content $model)
    {
        $this->model = $model;
        $this->defaultSorts = "id";
        $this->allowedSorts = ['-comment_num', '-give_num'];
        $this->allowedFilters = [
            "title",
            AllowedFilter::exact('channel_id'),
            AllowedFilter::callback('time', function ($query, $value) {
                $where = [
                    ['created_at', '>=', $value['start_time']],
                    ['created_at', '<', $value['end_time']],
                ];
                $query->where($where);
            })
        ];
    }
}
