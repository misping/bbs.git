<?php

namespace App\Http\Controllers\Backend\Comment;

use App\Application\Comment\Models\Comment;
use App\Application\Content\Client;

use Spatie\QueryBuilder\AllowedFilter;

class CommentController extends Client
{
    public function __construct(Comment $model)
    {
        $this->model = $model;
        $this->defaultSorts = "id";
        $this->allowedSorts = ['-recovery_num', '-give_num'];
        $this->allowedFilters = [
            "comment",
            AllowedFilter::exact('content_id'),
            AllowedFilter::callback('time', function ($query, $value) {
                $where = [
                    ['created_at', '>=', $value['start_time']],
                    ['created_at', '<', $value['end_time']],
                ];
                $query->where($where);
            })
        ];
    }
}
