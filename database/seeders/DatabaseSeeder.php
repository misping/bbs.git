<?php


use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Laravel\Passport\ClientRepository;
use Spatie\Permission\Models\Permission;
use Composer\Support\Auth\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::createAdminUser('bluedot', 'Bluedot@2022');

        Permission::create(['name' => 'channel', 'label' => '频道管理']);
        Permission::create(['name' => 'content', 'label' => '内容管理']);
        Permission::create(['name' => 'comment', 'label' => '评论管理']);

        $client = new ClientRepository();
        $client->createPasswordGrantClient(null, 'Default Tenant Client', '');
        $client->createPersonalAccessClient(null, 'Default Tenant Personal Client', '');
    }
}
