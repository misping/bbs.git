## About Bluedot Laravel

laravel for bluedot 脚手架

## Docs

#### Install

```
composer create-project bbs/laravel {项目文件夹}
```

#### Initialization

 
1. 上线后需把.env 自动生成的配置信息同步到.env.production，管理方式自行安排
2. 修改本地 .env APP_ENV=production 配置数据库信息 pgsql
#### 初始化数据库

```
php artisan migrate
```

#### 初始化数据

```
php artisan db:seed
```
#### 本地开发

 


#### 本地开发

```
 
  php -S 0.0.0.0:8000 -t public
```

#### 功能说明

测试公众号 授权回调之后需要手动修改127.0.0.1 改成本地启动的地址
-   微信功能配置 => [EasyWeChat](https://easywechat.com/5.x/)
-   权限配置 => [Laravel-permission](https://spatie.be/docs/laravel-permission/v5/introduction)
-    接口文档=>https://docs.apipost.cn/preview/551dd08d4b349f74/9a78ef85f6b27a49
